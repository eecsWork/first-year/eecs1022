# EECS 1022 Labs

## [Lab 1 - BMI](https://gitlab.com/eecsWork/first-year/eecs1022/tree/master/BMI/app/src/main)
Calculates BMI given the mass and height of a person.

## [Lab 2 - MCalc](https://gitlab.com/eecsWork/first-year/eecs1022/tree/master/MCalc/app/src/main)
Calculates mortgage rates given a principal amount, amortization persion, and interest rate.

## [Lab 3 - MCalcPro](https://gitlab.com/eecsWork/first-year/eecs1022/tree/master/MCalcPro/app/src/main)
Calculates mortgage rates given a principal amount, amortization persion, and interest rate using the i2c API.

## [Lab 4 - KryptoNote](https://gitlab.com/eecsWork/first-year/eecs1022/tree/master/KryptoNote/app/src/main)
Encrypts notes given a key and can save/load said notes.