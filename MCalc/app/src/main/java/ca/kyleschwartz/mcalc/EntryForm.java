package ca.kyleschwartz.mcalc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class EntryForm extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mortgage_layout);
    }

    public void buttonClick(View v) {
        EditText principle = (EditText) findViewById(R.id.prin);
        String prin = principle.getText().toString();

        EditText interest = (EditText) findViewById(R.id.inte);
        String inte = interest.getText().toString();

        EditText amortization = (EditText) findViewById(R.id.amor);
        String amor = amortization.getText().toString();

        try {
            MortgageModel myModel = new MortgageModel(prin, amor, inte);
            ((TextView) findViewById(R.id.textView4)).setText(myModel.computePayment());
        } catch (NumberFormatException e) {
        }
    }

    public void outstandingClick(View v) {
        EditText principle = (EditText) findViewById(R.id.prin);
        String prin = principle.getText().toString();

        EditText interest = (EditText) findViewById(R.id.inte);
        String inte = interest.getText().toString();

        EditText amortization = (EditText) findViewById(R.id.amor);
        String amor = amortization.getText().toString();

        try {
            MortgageModel myModel = new MortgageModel(prin, amor, inte);
            ((TextView) findViewById(R.id.textView5)).setText(myModel.outstandingAfter(5 * 12));
        } catch (NumberFormatException e) {
        }
    }
}