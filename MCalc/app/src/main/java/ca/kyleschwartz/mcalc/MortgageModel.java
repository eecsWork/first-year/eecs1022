package ca.kyleschwartz.mcalc;

import java.util.Locale;

public class MortgageModel {
    private int principle;
    private double amortization;
    private double interest;

    public MortgageModel(String p, String a, String i) {
        this.principle = Integer.parseInt(p);
        this.amortization = Double.parseDouble(a);
        this.interest = Double.parseDouble(i);
    }

    public String computePayment() {
        int p = this.principle;
        double r = this.interest / 100 / 12;
        double n = this.amortization * 12;

        double total = (double) Math.round((r * p) / (1 - Math.pow((1 + r), (-n))) * 100) / 100;

//Dollars, cents, and thousands separator
        return String.format(Locale.CANADA, "$%,.2f", total);

//        Only Dollars
//        return "$" + String.valueOf((int) Math.floor(total));
    }

    public String outstandingAfter(int x) {
        int p = this.principle;
        double r = this.interest / 100 / 12;
        String payment = computePayment().replace(",", "");
        payment = payment.substring(1);


        double total = p - (Double.parseDouble(payment) / r - p) * (Math.pow((1 + r), x) - 1);
        return String.format(Locale.CANADA, "$%,.0f", total);
    }

    public static void main(String[] args) {
        MortgageModel myModel = new MortgageModel("700000", "25", "2.75");
        System.out.println(myModel.outstandingAfter(60));
        System.out.println(myModel.computePayment());
    }
}
