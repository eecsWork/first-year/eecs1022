package ca.kyleschwartz.kryptonote;

public class KryptoNoteModel {
    private String key;
    private String note;
    private static final String ALPHABET = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public KryptoNoteModel(String k, String n) {
        this.key = k;
        this.note = n;
    }

    private String makePad(String n) {
        String pad = this.key;
        for (; pad.length() < n.length(); )
            pad += this.key;
        return pad;
    }

    public String encrypt() {
        String pad = makePad(this.note);
        String result = "";
        for (int i = 0; i < note.length(); i++) {
            String c = note.substring(i, i + 1);
            int position = ALPHABET.indexOf(c);
            int shift = Integer.parseInt(pad.substring(i, i + 1));
            int newPosition = (position + shift) % ALPHABET.length();
            result = result + ALPHABET.substring(newPosition, newPosition + 1);
        }
        return result;
    }

    public String decrypt() {
        String pad = makePad(this.note);
        String result = "";
        for (int i = 0; i < note.length(); i++) {
            String c = note.substring(i, i + 1);
            int position = ALPHABET.indexOf(c);
            int shift = Integer.parseInt(pad.substring(i, i + 1));
            int newPosition = (position - shift) % ALPHABET.length();
            result = result + ALPHABET.substring(newPosition, newPosition + 1);
        }
        return result;
    }

}
