package ca.kyleschwartz.bmi;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class BMIModel {
    private double weight;
    private double height;

    public BMIModel(String w, String h) {
        this.weight = Double.parseDouble(w);
        this.height = Double.parseDouble(h);
    }

    public String getBMI() {
        double index = this.weight / (this.height * this.height);
        String result = String.format("%.1f", index);
        return result;
    }

    public String toPound() {
        int index = (int) Math.round(this.weight / 0.454);
        String result = String.valueOf(index);
        return result;
    }

    public String toFeetinch() {
        int feet = (int) Math.floor(this.height * 3.281);
        int inches = (int) Math.round((this.height - feet / 3.281) * 39.37);
        String result = feet + "'" + inches + '"';
        return result;
    }

    public static void main(String[] args) {
        BMIModel myModel = new BMIModel("77", "1.78");
        System.out.println(myModel.toFeetinch());
    }
}
