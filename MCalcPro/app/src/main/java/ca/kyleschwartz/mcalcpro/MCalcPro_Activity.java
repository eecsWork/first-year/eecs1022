package ca.kyleschwartz.mcalcpro;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import ca.roumani.i2c.MPro;

public class MCalcPro_Activity extends AppCompatActivity implements TextToSpeech.OnInitListener, SensorEventListener {

    private TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.tts = new TextToSpeech(this, this);
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sm.registerListener(this, sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void onInit(int initStatus) {
        this.tts.setLanguage(Locale.US);
    }

    @Override
    public void onAccuracyChanged(Sensor arg0, int arg1) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        double ax = event.values[0];
        double ay = event.values[1];
        double az = event.values[2];
        double a = Math.sqrt(ax * ax + ay * ay + az * az);
        if (Math.abs(a) > 20) {
            ((EditText) findViewById(R.id.pBox)).setText("");
            ((EditText) findViewById(R.id.aBox)).setText("");
            ((EditText) findViewById(R.id.iBox)).setText("");
            ((TextView) findViewById(R.id.output)).setText("");
        }
    }


    public void onClick(View v) {
        try {
            MPro mp = new MPro();

            mp.setPrinciple(((EditText) findViewById(R.id.pBox)).getText().toString());
            mp.setAmortization(((EditText) findViewById(R.id.aBox)).getText().toString());
            mp.setInterest(((EditText) findViewById(R.id.iBox)).getText().toString());

            String s = "Monthly Payment = " + mp.computePayment("%,.2f");
            s += "\n\n";
            s += "By making these payments monthly for 20 years, the mortgage will be paid in full. But if you terminate the mortgage on it's nth anniversary, the balance still owing depends on n as shown below:";
            s += "\n\n";
            s += String.format("%8s", "n") + String.format("%16s", "Balance");
            for (int x = 0; x < 6; x++) {
                s += "\n\n";
                s += String.format(Locale.CANADA, "%8d", x) + mp.outstandingAfter(x, "%,16.0f");
            }
            for (int x = 10; x < 25; x += 5) {
                s += "\n\n";
                s += String.format(Locale.CANADA, "%8d", x) + mp.outstandingAfter(x, "%,16.0f");
            }

            ((TextView) findViewById(R.id.output)).setText(s);

            s = "Monthly Payment = " + mp.computePayment("%,.2f");
            tts.speak(s, TextToSpeech.QUEUE_FLUSH, null);

        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
