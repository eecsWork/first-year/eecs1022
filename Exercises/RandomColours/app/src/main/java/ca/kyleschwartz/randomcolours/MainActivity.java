package ca.kyleschwartz.randomcolours;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonClicked(View v) {
        double r = rand();
        double g = rand();
        double b = rand();
        ((TextView) findViewById(R.id.button)).setBackgroundColor(Color.rgb((int) r, (int) g, (int) b));
        String bottom = "R=" + Math.round(r / 255 * 100) + "%, G=" + Math.round(g / 255 * 100) + "%, B=" + Math.round(b / 255 * 100) + "%";
        ((TextView) findViewById(R.id.textView2)).setText(bottom);
    }

    private double rand() {
        return Math.floor(Math.random() * 256);
    }
}
