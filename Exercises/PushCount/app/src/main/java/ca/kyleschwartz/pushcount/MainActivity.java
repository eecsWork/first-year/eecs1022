package ca.kyleschwartz.pushcount;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonClicked(View v) {
        CharSequence cs = ((TextView) findViewById(R.id.textView3)).getText();
        int num = Integer.parseInt(cs.toString()) + 1;
        ((TextView) findViewById(R.id.textView3)).setText(String.valueOf(num));
    }
}
