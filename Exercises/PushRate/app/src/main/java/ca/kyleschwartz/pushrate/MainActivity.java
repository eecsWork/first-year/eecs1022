package ca.kyleschwartz.pushrate;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import ca.roumani.i2c.Utility;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonClicked(View v) {
        String text = Math.round(Utility.mark() / 1000) + " sec since last push";
        ((TextView) findViewById(R.id.textView)).setText(text);
    }
}
